#!/usr/bin/env bash
# 
# Setup a host for deployment
# 

# errors are fatal
set -e

# parse arguments
args=$(getopt p:n: $*)
# set arguments
set -- $args
# progess arguments
for i; do
  case "$i"
  in
    -p)
      install_base="$2"; shift;
      shift;;
    -n)
      target_name="$2"; shift;
      shift;;
    --)
      shift; break;;
  esac
done

# make sure we have a target name
if [ -z "$target_name" ]; then
  echo "No target name provided; use -n <name>"
  exit -1
fi

# if an install prefix is defined normalize it
if [ ! -z "$install_base" ]; then
  install_base=${install_base%/}
fi

# make sure the install base has a trailing slash
install_base="${install_base}/"

# setup scaffolding
mkdir -p "${install_base}usr/svc"
mkdir -p "${install_base}var/svc"
mkdir -p "${install_base}var/svc/$target_name"
mkdir -p "${install_base}etc/svc"
mkdir -p "${install_base}etc/svc/$target_name"

# setup env
if [ ! -f "${install_base}etc/svc/$target_name/env" ]; then
  touch "${install_base}etc/svc/$target_name/env"
fi


#!/usr/bin/env bash

# where am i
ME="$0"
ME_HOME=$(dirname "$0")
ME_HOME=$(cd "$ME_HOME" && pwd)

# root
DEPLOYPACK_ROOT="$HOME"
# repo name
DEPLOYPACK_NAME=".deploypack"
# well known deploy pack location
DEPLOYPACK_BASE="$DEPLOYPACK_ROOT"
# well known deploy pack location
DEPLOYPACK_REPO="/$DEPLOYPACK_BASE/$DEPLOYPACK_NAME"

# look for our friends
if [ -x "$ME_HOME/install.sh" ]; then
  # just use the one next to this
  deploypack_home="$ME_HOME"
elif [ -d "$DEPLOYPACK_REPO" ]; then
  (cd "$DEPLOYPACK_REPO" && git pull)
  deploypack_home="$DEPLOYPACK_REPO"
else
  (cd "$DEPLOYPACK_BASE" && git clone https://bww@bitbucket.org/bww/deploypack.git "$DEPLOYPACK_NAME")
  deploypack_home="$DEPLOYPACK_REPO"
fi

# source in erun
. "$deploypack_home/_base.sh"
# do our install
erun "$deploypack_home/install.sh" $*


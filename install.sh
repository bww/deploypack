#!/usr/bin/env bash

# where am i
ME="$0"
ME_HOME=$(dirname "$0")
ME_HOME=$(cd "$ME_HOME" && pwd)

# check for a sudo user in the environment
if [ ! -z "$DEPLOYPACK_USER" ]; then
  sudo_user="$DEPLOYPACK_USER"
fi

# parse arguments
args=$(getopt a:A:p:n:i:u:E: $*)
# set arguments
set -- $args
# progess arguments
for i; do
  case "$i"
  in
    -p)
      install_base="$2"; shift;
      shift;;
    -a)
      target_archive="$2"; shift;
      shift;;
    -A)
      target_archive_url="$2"; shift;
      shift;;
    -n)
      target_name="$2"; shift;
      shift;;
    -i)
      identity_path="$2"; shift;
      shift;;
    -u)
      sudo_user="$2"; shift;
      shift;;
    -E)
      export_variables="$export_variables -E '$2'"; shift;
      shift;;
    --)
      shift; break;;
  esac
done

# make sure we have an archive to install
if [[ -z "$target_archive" && -z "$target_archive_url" ]]; then
  echo "No archive provided; use -a <archive> or -A <url>"
  exit -1
fi

# make sure we have a target name
if [ -z "$target_name" ]; then
  echo "No target name provided; use -n <name>"
  exit -1
fi

# if we have an identity, setup the flag
if [ ! -z "$identity_path" ]; then
  identity_flags="-i $identity_path"
fi

# if no sudo user is defined, use root
if [ -z "$sudo_user" ]; then
  sudo_user="root"
fi

# if an install prefix is defined normalize it
if [ ! -z "$install_base" ]; then
  install_base=${install_base%/}
fi

# make sure the install base has a trailing slash
install_base="${install_base}/"

# errors are fatal
set -e

# source in shared routines
. "$ME_HOME/_base.sh"

# install
install_target () {
  
  if [ -z "$1" ]; then
    echo "No host provided"
    return -1
  else
    target_host="$1"
    shift
  fi
  
  # note the host
  echo "-----> $target_host"
  
  # archive name
  archive_name=$(basename "$target_archive")
  # our install path
  install_path="${install_base}usr/svc/$target_name"
  # prefix
  build_prefix="com.thisismess.Buildpack"
  # key
  build_key=$(uuidgen)
  
  # our working directory
  build_dir="/tmp/$build_prefix.$build_key"
  # note it
  echo "-----> $build_dir"
  
  # create our temp directory
  erun ssh -q -T -o UserKnownHostsFile=/dev/null -o StrictHostKeyChecking=no $identity_flags "$target_host" mkdir -p "$build_dir"
  # copy our setup script and possibly our archive over
  erun scp -o UserKnownHostsFile=/dev/null -o StrictHostKeyChecking=no $identity_flags "$ME_HOME/_base.sh" "$ME_HOME/_setup.sh" "$ME_HOME/_install.sh" "$target_archive" "$target_host:$build_dir"
  
  # setup and install
  if [ ! -z "$target_archive" ]; then
    archive_flags="-a '$archive_name'"
  elif [ ! -z "$target_archive_url" ]; then
    archive_flags="-A '$target_archive_url'"
  else
    exit -1
  fi
  
  # setup the host
  erun ssh -q -T -o UserKnownHostsFile=/dev/null -o StrictHostKeyChecking=no $identity_flags "$target_host" sudo -u "$sudo_user" "$build_dir/_setup.sh" -n "$target_name" -p "$install_base"
  # install the target
  erun ssh -q -T -o UserKnownHostsFile=/dev/null -o StrictHostKeyChecking=no $identity_flags "$target_host" sudo -u "$sudo_user" "$build_dir/_install.sh" -o "$install_path" -n "$target_name" -b "$build_dir" -p "$install_base" $archive_flags $export_variables
  
  # done
  if [ $? -eq "0" ]; then
    echo "-----> Ok."
  else
    echo "-----> Failed."
  fi
  
}

# process target hosts
while [ ! -z "$1" ]; do
  # deploy to this host
  target_host="$1"; shift
  # do the install
  install_target "$target_host"
  # make sure everything worked out
  if [ $? -ne "0" ]; then
    exit $?
  fi
done




#!/usr/bin/env bash

# where am i
ME="$0"
ME_HOME=$(dirname "$0")
ME_HOME=$(cd "$ME_HOME" && pwd)

# install AWS tools
install_aws_tools () {
  
  # install easy_install
  if [ -z $(which easy_install) ]; then
    erun aptitude install -y python-setuptools
  fi
  
  # install pip
  if [ -z $(which pip) ]; then
    erun easy_install pip
  fi
  
  # install AWS CLI
  if [ -z $(which aws) ]; then
    erun pip install awscli
  fi
  
}

# download a URL to file
download_url () {
  if [ ! -z $(echo "$1" | grep -E "^s3:\/\/") ]; then
    install_aws_tools
    aws s3 cp "$1" "$2"
  else
    curl -fSL --progress-bar "$1" > "$2"
  fi
}

# parse arguments
args=$(getopt p:o:a:A:n:b:E: $*)
# set arguments
set -- $args
# progess arguments
for i; do
  case "$i"
  in
    -p)
      install_base="$2"; shift;
      shift;;
    -o)
      install_path="$2"; shift;
      shift;;
    -a)
      archive_name="$2"; shift;
      shift;;
    -A)
      archive_url="$2"; shift;
      shift;;
    -n)
      target_name="$2"; shift;
      shift;;
    -b)
      build_dir="$2"; shift;
      shift;;
    -E)
      export "$2"; shift;
      shift;;
    --)
      shift; break;;
  esac
done

# make sure we have an archive name
if [[ -z "$archive_name" && -z "$archive_url" ]]; then
  echo "No archive name provided; use -a <archive> or -A <url>"
  exit -1
fi

# make sure we have a target name
if [ -z "$target_name" ]; then
  echo "No target name provided; use -n <name>"
  exit -1
fi

# make sure we have a build directory
if [ -z "$build_dir" ]; then
  echo "No build directory provided; use -b <path>"
  exit -1
fi

# make sure we have an install path
if [ -z "$install_path" ]; then
  echo "No install path provided; use -o <path>"
  exit -1
fi

# if an install prefix is defined normalize it
if [ ! -z "$install_base" ]; then
  install_base=${install_base%/}
fi

# make sure the install base has a trailing slash
install_base="${install_base}/"

# errors are fatal
set -e

# source in shared routines
. "$build_dir/_base.sh"

# move into our build directory
erun cd "$build_dir"
# archive directory
archive_dir="$build_dir/package"
# backup directory
backup_dir="$build_dir/backup"
# install name
install_name=$(basename "$install_path")
# our install script
install_script="$install_path/etc/install.sh"

# download our archive, if necessary
if [ ! -z "$archive_url" ]; then
  archive_name="package.tar.gz"
  download_url "$archive_url" "$build_dir/$archive_name"
fi

# make our output directory
mkdir -p "$archive_dir"
# extract the archive
erun tar -zxf "$archive_name" -C "$archive_dir" --strip-components=1
# our host init script
init_script="$archive_dir/etc/init.sh"

# run host init scripts, if we have any
if [ -f "$init_script" ]; then
  echo "-----> Running host init script"
  erun export DEPLOY_TARGET="$target_name"
  erun export DEPLOY_BUILD_DIR="$build_dir"
  erun export DEPLOY_INSTALL_PATH="$install_path"
  erun export DEPLOY_INSTALL_PREFIX="$install_base"
  erun export DEPLOY_CONFIG_PREFIX="${install_base}etc/svc/$target_name"
  erun export DEPLOY_ETC="${install_base}etc/svc"
  erun export DEPLOY_USR="${install_base}usr/svc"
  erun export DEPLOY_VAR="${install_base}var/svc"
  erun bash "$init_script"
else
  echo "-----> No host init script found"
fi

# set aside the previous build, if we have one
if [ -d "$install_path" ]; then
  echo "-----> Moving old version aside"
  erun mkdir -p "$backup_dir"
  erun mv "$install_path" "$backup_dir/"
fi

# move the package into place
echo "-----> Installing"
erun mv "$archive_dir" "$install_path"

# run installation scripts, if we have any
if [ -f "$install_script" ]; then
  echo "-----> Running install script"
  erun export DEPLOY_TARGET="$target_name"
  erun export DEPLOY_BUILD_DIR="$build_dir"
  erun export DEPLOY_INSTALL_PATH="$install_path"
  erun export DEPLOY_INSTALL_PREFIX="$install_base"
  erun export DEPLOY_CONFIG_PREFIX="${install_base}etc/svc/$target_name"
  erun export DEPLOY_ETC="${install_base}etc/svc"
  erun export DEPLOY_USR="${install_base}usr/svc"
  erun export DEPLOY_VAR="${install_base}var/svc"
  erun bash "$install_script"
else
  echo "-----> No install script found."
fi

# make sure the install went ok
if [ $? -ne "0" ]; then
  echo "-----> Install failed; rolling back"
  erun rm -r "$install_path"
  erun mv "$backup_dir/$install_name" "$install_path"
  exit -1
fi

# clean up
echo "-----> Cleaning up"
erun rm -rf "$build_dir"


